package com.task12_IO_NIO.model.shipWithDroids;

import java.io.Serializable;
import java.util.Objects;

public class Ship implements Serializable {

  private int size;
  private String color;
  private Droid droid;

  public void resizing(){
    if (this.size == 10) this.size = 20;
  }

  public Ship() {
  }

  public Ship(int size, String color, Droid droid) {
    this.size = size;
    this.color = color;
    this.droid = droid;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public Droid getDroid() {
    return droid;
  }

  public void setDroid(Droid droid) {
    this.droid = droid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ship ship = (Ship) o;
    return size == ship.size &&
        Objects.equals(color, ship.color) &&
        Objects.equals(droid, ship.droid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(size, color, droid);
  }

  @Override
  public String toString() {
    return "Ship{" +
        "size=" + size +
        ", color='" + color + '\'' +
        ", droid=" + droid +
        '}';
  }
}
