package com.task12_IO_NIO.model.shipWithDroids;

import java.io.Serializable;
import java.util.Objects;

public class Droid implements Serializable {

  private String name;
  private int power;
  private transient String mood;

  public void boostPower(int additionalPower){
    this.power = this.power + additionalPower;
  }

  public Droid() {
  }

  public Droid(String name, int power, String mood) {
    this.name = name;
    this.power = power;
    this.mood = mood;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPower() {
    return power;
  }

  public void setPower(int power) {
    this.power = power;
  }

  public String getMood() {
    return mood;
  }

  public void setMood(String mood) {
    this.mood = mood;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Droid droid = (Droid) o;
    return power == droid.power &&
        Objects.equals(name, droid.name) &&
        Objects.equals(mood, droid.mood);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, power, mood);
  }

  @Override
  public String toString() {
    return "Droid{" +
        "name='" + name + '\'' +
        ", power=" + power +
        ", mood='" + mood + '\'' +
        '}';
  }
}
