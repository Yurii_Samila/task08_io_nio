package com.task12_IO_NIO.model.directoryContent;

import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirectoryContent {

  public static void main(String[] args) throws IOException {

    Path path = Paths.get("e:/Epam/");
    Stream<Path> paths = Files.walk(path);
    List<Path> collect = paths.collect(Collectors.toList());
    for (int i = 0; i < collect.size(); i++) {
      Path path1 = collect.get(i);
      System.out.println(path1.getFileName() + " " + path1.getNameCount());
    }

  }
}
