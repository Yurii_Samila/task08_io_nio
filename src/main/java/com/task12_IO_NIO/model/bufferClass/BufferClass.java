package com.task12_IO_NIO.model.bufferClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class BufferClass {

  public static void main(String[] args) throws IOException {
    RandomAccessFile file = new RandomAccessFile("Text.txt", "rw");
    FileChannel fileChannel = file.getChannel();
    ByteBuffer buffer = ByteBuffer.allocate(256);
    int bytesRead = fileChannel.read(buffer);
    while (bytesRead != -1){
      buffer.flip();
      while (buffer.hasRemaining()){
        System.out.print((char)buffer.get());
      }
      buffer.clear();
      bytesRead = fileChannel.read(buffer);
    }
  file.close();
  }

}
