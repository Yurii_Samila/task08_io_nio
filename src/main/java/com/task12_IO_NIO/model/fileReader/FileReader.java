package com.task12_IO_NIO.model.fileReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StreamTokenizer;
import java.util.LinkedList;
import java.util.List;

public class FileReader {

  public static void main(String[] args) throws IOException {
    final String PATH = "Hello_World.java";

    List<String> comments = new LinkedList<>();
    StringBuilder sentence = new StringBuilder();

    StreamTokenizer streamTokenizer = new StreamTokenizer(new java.io.FileReader(PATH));
    streamTokenizer.ordinaryChar('/');
    streamTokenizer.eolIsSignificant(true);
    while (streamTokenizer.nextToken() != StreamTokenizer.TT_EOF){
      if (streamTokenizer.ttype == '/') {
        while (streamTokenizer.nextToken() != streamTokenizer.TT_EOL ){
          if (streamTokenizer.ttype == StreamTokenizer.TT_WORD){
            sentence.append(streamTokenizer.sval).append(" ");
          }
        }
        if (!sentence.toString().isEmpty()){
          comments.add(sentence.toString().trim());
          sentence = new StringBuilder();
        }
      }
    }
    System.out.println(comments);
  }
}
