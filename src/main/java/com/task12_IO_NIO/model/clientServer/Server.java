package com.task12_IO_NIO.model.clientServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {
  private  static final int BUFFER_SIZE = 1024;
  private static Selector selector;

  public static void main(String[] args) throws IOException {
    try {
      InetAddress hostIP = InetAddress.getLocalHost();
      int port = 9999;
      selector = Selector.open();
      ServerSocketChannel mySocket = ServerSocketChannel.open();
      ServerSocket serverSocket = mySocket.socket();
      InetSocketAddress address = new InetSocketAddress(hostIP, port);
      serverSocket.bind(address);

      mySocket.configureBlocking(false);
      int ops = mySocket.validOps();
      mySocket.register(selector, ops, null);
      while (true){
        selector.select();
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        Iterator<SelectionKey> iterator = selectedKeys.iterator();
        while (iterator.hasNext()){
          SelectionKey key = iterator.next();
          if (key.isAcceptable()){
            processAcceptEvent(mySocket, key);
          }else if (key.isReadable()){
            processReadEvent(key);
          }
          iterator.remove();
        }
      }
    }catch (IOException e){
      System.out.println("IOException");
    }
  }

  private static void processAcceptEvent(ServerSocketChannel mySocket, SelectionKey key)
      throws IOException {
    SocketChannel myClient = mySocket.accept();
    myClient.configureBlocking(false);
    myClient.register(selector, SelectionKey.OP_READ);
  }

  private static void processReadEvent(SelectionKey key) throws IOException {
    SocketChannel myClient = (SocketChannel) key.channel();
    ByteBuffer myBuffer = ByteBuffer.allocate(BUFFER_SIZE);
    myClient.read(myBuffer);
    String data = new String(myBuffer.array()).trim();
    if (data.length() > 0){
      logger(String.format("Massage received ", data));
      if (data.equalsIgnoreCase("exit")){
        myClient.close();
        logger("Server closed");
      }
    }
  }

  public static void logger(String st){
    System.out.println(st);
  }

}
