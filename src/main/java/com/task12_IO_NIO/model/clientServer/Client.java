package com.task12_IO_NIO.model.clientServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Client {
  private static final int BUFFER_SIZE = 1024;
  private static String[] messages = {"Some messages","*exit*"};

  public static void main(String[] args) throws IOException {
    try {
      int port = 9999;
      InetAddress hostIp = InetAddress.getLocalHost();
      InetSocketAddress myAddress = new InetSocketAddress(hostIp, port);
      SocketChannel myClient = SocketChannel.open();
      logger(String.format("Trying to connect to %s:%d...",
          myAddress.getHostName(), myAddress.getPort()));
      for (String msg:messages){
        ByteBuffer myBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        myBuffer.put(msg.getBytes());
        myBuffer.flip();
        int bytesWritten = myClient.write(myBuffer);
        logger(String
                .format("Sending Message...: %s\nbytesWritten...: %d",
                    msg, bytesWritten));

      }
    }catch (IOException e){
      System.out.println("IOException");
    }
  }
  public static void logger(String msg) {
    System.out.println(msg);
  }


}
