package com.task12_IO_NIO.model.myInputStream;

import java.io.IOException;
import java.io.InputStream;

public class MyInputStream extends InputStream {

  @Override
  public int read() throws IOException {
    return 0;
  }
}
