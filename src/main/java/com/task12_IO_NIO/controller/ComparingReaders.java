package com.task12_IO_NIO.controller;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ComparingReaders {

  public static void main(String[] args) throws IOException {
    final String PATH = "jdk-12.0.2_windows-x64_bin.exe";

    InputStream inputStream = new FileInputStream(PATH);
    int data = inputStream.read();
    long startTime = System.currentTimeMillis()/1000;
    while (data != -1){
      data = inputStream.read();
    }
    inputStream.close();
    long executingTime = System.currentTimeMillis()/1000 - startTime;
    System.out.println("Reading file by bits amounts:  " + executingTime + " seconds");

    DataInputStream dataInputStream1 = new DataInputStream(
        new BufferedInputStream(
            new FileInputStream(PATH)));
    startTime = System.currentTimeMillis()/1000;
    try {
      while (dataInputStream1.available() > 0){
        byte b = dataInputStream1.readByte();
      }
    }catch (EOFException e){
      System.out.println("EOFException");
    }
    dataInputStream1.close();
    executingTime = System.currentTimeMillis()/1000- startTime;
    System.out.println("Reading file by buffer without restrictions amounts:  " + executingTime + " seconds");

    int bufferSize1 = 1024;
    DataInputStream dataInputStream2 = new DataInputStream(
        new BufferedInputStream(
            new FileInputStream(PATH), bufferSize1));
    startTime = System.currentTimeMillis()/1000;
    try {
      while (dataInputStream2.available() > 0){
        byte b = dataInputStream2.readByte();
      }
    }catch (EOFException e){
      System.out.println("EOFExceptoin");
    }
    dataInputStream2.close();
    executingTime = System.currentTimeMillis()/1000 - startTime;
    System.out.println("Reading file by buffer with restriction by 1KB amounts:  " + executingTime + " seconds");

    int bufferSize2 = 1;
    DataInputStream dataInputStream3 = new DataInputStream(
        new BufferedInputStream(
            new FileInputStream(PATH), bufferSize2));
    startTime = System.currentTimeMillis()/1000;
    try {
      while (dataInputStream3.available() > 0){
        byte b = dataInputStream3.readByte();
      }
    }catch (EOFException e){
      System.out.println("EOFExceptoin");
    }
    dataInputStream3.close();
    executingTime = System.currentTimeMillis()/1000 - startTime;
    System.out.println("Reading file by buffer with restriction by 1 byte amounts:  " + executingTime + " seconds");
  }

}
