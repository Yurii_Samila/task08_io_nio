package com.task12_IO_NIO.controller;


import com.task12_IO_NIO.model.shipWithDroids.Droid;
import com.task12_IO_NIO.model.shipWithDroids.Ship;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ShipController {

  private static final String PATH = "ship.txt";

  public static void serialize(){
    Ship ship1 = new Ship(20, "Yellow", new Droid("Toro", 250, "Good"));

    try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(PATH))) {
    outputStream.writeObject(ship1);
    } catch (IOException e){
      System.out.println("IOException");
    }
  }

  private static void deserialize() throws ClassNotFoundException {
    try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(PATH))){
    Ship deserializedShip = (Ship) objectInputStream.readObject();
      System.out.println(deserializedShip);
    }catch (IOException e){
      System.out.println("IOEception while deserializing");
    }
  }
  public static void main(String[] args) throws ClassNotFoundException {
    serialize();
    deserialize();
  }
}
